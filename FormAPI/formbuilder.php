<?php

namespace FormAPI;

class FormBuilder {

   private $dependencyInjector;
   public function __construct(\FormAPI\Lib\DependencyInjector $dependencyInjector) {
      $this->dependencyInjector = $dependencyInjector;
   }

   public function getForm($formId) {
      
   }
   
   private function buildForm(array $formArray = array()) {
      $formState = new FormState();
      // populate the form array.
      
      // Recurse through the form, building it.  FormState is passed in for value retrieval.
      $this->processForm($formArray, $formState);
   }
   
   private function processForm(array $formArray = array(), \FormAPI\Lib\FormState $formState) {
      // Get the children first
      $children = $this->dependencyInjector->getDependency('utility')->elementChildren($formArray);
      
      // Peel off the children.
      $formProperties = array_diff_key($formArray, $children);
      $form = new \FormAPI\Lib\Elements\Form($this->dependencyInjector, array(), $formProperties);
      foreach($children as $child) {
         $this->processElement($form, $child, $formState);
      }
   }
   
   private function processElement(\FormAPI\Lib\Elements\FormElement $parent, array $elementArray = array(), \FormAPI\Lib\FormState $formState) {
      
      // Get the children first
      $children = $this->dependencyInjector->getDependency('utility')->elementChildren($elementArray);
      
      // Peel off the children.
      $elementProperties = array_diff_key($elementArray, $children);
      
      // Get the parents.  The parents are the $parent's parents + the parent.
      $parents = $parent->getParents() + $parent->getName();
      $element = new $elementArray['#element']($this->dependencyInjector, $parents, $elementProperties);
      // Set values passed in via the formState.
      $element->valueCallback($elementArray, array(), $formState);
      foreach($children as $child) {
         $this->processElement($element, $child, $formState);
      }
      
      $parent->addChild($element);
   }
   private function renderForm(array $formArray = array()) {
      $output = $formArray['#object']->render();
   }
}