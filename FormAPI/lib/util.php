<?php

namespace FormAPI\Lib;

class Util
{

   public function renderAttributes($attributes)
   {
      foreach ($attributes as $attribute => &$data) {
         $data = implode(' ', (array) $data);
         $data = $attribute . '="' . $data . '"';
      }
      return !empty($attributes) ? ' ' . implode(' ', $attributes) : '';
   }
   
   public function formatName(array $parents = array(), $name = '') {
      return 'form[' . implode('][', $parents) . '][' . $name . ']';
   }
   
   public function formatId(array $parents = array(), $name = '') {
      return 'form_' . implode('_', $parents) . '_' . $name;
   }
   
   public function elementChildren($element) {
      $children = array();
      foreach($element as $key => $property) {
         if(strpos($key, '#') !== 0) {
            $children = $property;
         }
      }
      return $children;
   }
}