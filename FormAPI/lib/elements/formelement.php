<?php

namespace FormAPI\Lib\Elements;


abstract class FormElement {
   
   protected $properties = array();
   protected $dependencyInjector;
   protected $parents = array();
   protected $isContainer = false;
   public final function __construct(\FormAPI\Lib\DependencyInjector $dependencyInjector, array $parents = array(), array $instanceProperties = array()) {
      $this->dependencyInjector = $dependencyInjector;
      $this->parents = $parents;
      
      $this->properties = array_merge_recursive($this->getProperties(), $instanceProperties);
      if(empty($this->properties['#name'])) {
         throw new \InvalidArgumentException('Form elements must be named.');
      }
   }
   
   public function addChild(\FormAPI\Lib\FormElement $child) {
      if($this->isContainer) {
         $this->children[] = $child;
      }
   }
   public final function getName() {
      return $this->properties['#name'];
   }
   
   public final function getParents() {
      return $this->parents;
   }
   abstract public function getProperties();
   
   abstract protected function preRender();
   
   public final function render() {
      
      $this->preRender();
      $this->properties['#attributes']['name'] = $this->dependencyInjector->getDependency('utility')->formatName($this->parents, $this->properties['#name']);
      $this->properties['#attributes']['id'] = $this->dependencyInjector->getDependency('utility')->formatId($this->parents, $this->properties['#name']);
      $renderedAttributes = $this->dependencyInjector->getDependency('utility')->renderAttributes(!empty($this->properties['#attributes']) ? $this->properties['#attributes'] : array());
      $output = '';
      $output .= "<{$this->tagName} $renderedAttributes>";
      if($this->isContainer) {
         if(!empty($this->children)) {
            foreach($this->children as $child) {
               $output .= $child->render();
            }
         }
      } else {
         if(!empty($this->value)) {
            $output .= $this->value;
         }
      }
      $output .= "</{$this->tagName}>";
      return $output;
   }
   abstract public function valueCallback($element, $input, \FormAPI\Lib\FormState $formState);
}
