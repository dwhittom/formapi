<?php

namespace FormAPI\Lib\Elements\Composite;

abstract class CompositeElement extends FormElement {
   
   abstract public function element();
}
