<?php

namespace FormAPI\Lib\Elements;

class TextField extends \FormAPI\Lib\Elements\Input {
   
   private $defaults = array(
      '#tag' => 'input',
      '#attributes' => array(
         'type' => 'textfield',
      )
   );
   
   protected function getProperties()
   {
      return $this->properties + $this->defaults + parent::getProperties();
   }

   public function valueCallback($element, $input, \FormAPI\Lib\FormState $formState)
   {
      
   }

}
