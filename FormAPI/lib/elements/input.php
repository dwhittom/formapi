<?php

namespace FormAPI\Lib\Elements;

class Input extends \FormAPI\Lib\Elements\FormElement {

   private $defaults = array(
      '#tag' => 'input',
   );
   public function getProperties()
   {
      $this->properties + $this->defaults;
   }

   public function valueCallback($element, $input, \FormAPI\Lib\FormState $formState)
   {
      
   }

   protected function preRender()
   {
      // Do nothing.
   }

}