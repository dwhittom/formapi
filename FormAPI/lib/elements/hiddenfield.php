<?php

namespace FormAPI\Lib\Elements;

class HiddenField extends \FormAPI\Lib\Elements\Input {
   
   private $defaults = array(
      '#tag' => 'input',
      '#attributes' => array(
         'type' => 'hidden',
      )
   );
   protected function getProperties() {
      return $this->properties + $this->defaults + parent::getProperties();
   }
   
   public function valueCallback($element, $input, \FormAPI\Lib\FormState $formState)
   {
      
   }
   
   protected function preRender() {
      if(!empty($this->value)) {
         // serialize and encrypt the value.
         $serializedValue = serialize($this->value);
         $this->properties['#attributes']['value'] = $serializedValue;
      }
   }
}