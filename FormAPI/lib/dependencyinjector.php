<?php

namespace FormAPI\Lib;

class DependencyInjector {
   private $dependencies = array();
   
   public function __construct() {
      
   }
   public function setDependency($dependencyName, $object) {
      $this->dependencies[$dependencyName] = $object;
   }
   
   public function getDependency($dependencyName) {
      return $this->dependencies[$dependencyName];
   }
}

